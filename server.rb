require 'sinatra'
require_relative 'app'


enable :sessions

app = App.new

pages = {
  :bio => { title: "Biografie"},
  :gallery => { title: "Galerie",
            file: "galerie" ,
            data: {
              galleries: {
                oct: {
                  title: "die okopussgeschichte",
                  folder: "oct",
                  image_count: 5,
                  #image_postfix: "_oct.jpg",
                  alts: [
                    "die oktopusgeschichte teil I _ Mixed Media auf Leinwand_160x100",
                    "die oktopusgeschichte teil II _ Mixed Media auf Leinwand_160x100",
                    "die oktopusgeschichte teil III _ Mixed Media auf Leinwand_160x100",
                    "die oktopusgeschichte teil IV _ Mixed Media auf Leinwand_160x100",
                    "die oktopusgeschichte teil V _ Mixed Media auf Leinwand_160x100",
                   ],
									text: <<-EOT
      Die Reihe der oktopusgeschichte ist eine Auseinandersetzung mit dem Gedicht „Der Oktopus“ von L.Stüdemann. In den Mixed Media auf Leinwandarbeiten wurden zwar immer wiederkehrend Fragmente der verbalen Ausformung des Gedichtes verwand. Jedoch nicht die dem Gedicht innewohnende Thematik oder einzelne Worte und deren Bedeutung sind der Kern der Bilder. Die Anordnung der Konsonanten und Vokale, wie diese zueinander stehen und aufeinander folgen, wurden von Stüdemann so gesetzt, dass sie beim laut lesen oder hören ein tiefes, weichwohlig- melancholisches Gefühl erzeugen. Dieses Gefühl ist das zentrale Sujet der Reihe „die oktopusgeschichte“.
      <p>
      der oktopus<br /><br />

      der oktopus schreibt die geschichte nieder. <br />
      tintene worte die ihm der wind in einer tosenden umarmung entgegenschlug.<br />
      niemand wusste was diese worte in sich trugen,<br />
      keiner brach den schwur des zauberhaften.<br />
      so schaukelte die flaschenpost auf den wogen der welt an orte nimmermüden staunens,<br />
      um den keimling des obskuren an die ufer zu tragen.<br />
      wurzelnde laute fliegende freude.<br />
      verzehrende verzweiflung …<br />
      nimm die stimmgabel vom klang hinfort und setz sie auf den goldenen tron des höchsten gipfels,<br />
      küss sie und leite so den sternenklang in moosweicher wohligkeit ein, auf dass das mitternachtsblaue gestirn sich zur ruhe spanne.<br />
      L. Stüdemann
      </p>
EOT
                    
                },
                otm: {
                  title: "oneway to mars",
                  folder: "otm",
                  image_count: 8,
                  #image_postfix: "_oct.jpg",
                  alts: [
                    "oneway to mars I _ Acryl auf Leinwand_120x80",
                    "oneway to mars II _ Acryl auf Leinwand_120x80",
                    "oneway to mars III _ Acryl auf Leinwand_80x120",
                    "oneway to mars IV _ Acryl auf Leinwand_120x150",
                    "oneway to mars V _ Acryl auf Leinwand _120x150",
                    "oneway to mars VI _ Acryl auf Leinwand_80x120",
                    "oneway to mars VII _ Acryl auf Leinwand_80x120",
                    "oneway to mars VIII _ Acryl auf Leinwand_80x120",
                   ],
									text: <<-EOT
Die Reihe „oneway to mars“ ist ein weiterer Versuch dem Zufall habhaft zu werden. Damit ist gemeint, dass die Maltechnik zwar tachistisch ist und somit vieles Grundsätzliches dem Zufall überlassen scheint, aber eine intensive manuelle Einwirkung auf die Farbaufträge elementarer Bestandteil ist. So kommt eine Maltechnik zum tragen die als eine konsequente Weiterentwicklung des tachistischen actionpainting verstanden werden kann.
Die Bilder der Reihe stehen in einem immerwährenden Spannungsverhältnis in dem sich Bild und Maler in einem Mächtegleichgewicht gegenüberstehen. Genauso wie der Mensch im Hinblick auf geplante Marsmissionen nicht wissen kann auf was genau er sich dort einlässt und kleinste Fehler katastrophale Folgen haben können. 

EOT
                    
                },
                k: {
                  title: "kallus",
                  folder: "kallus",
                  image_count: 5,
                  #image_postfix: "_oct.jpg",
                  alts: [
                    "kallus I _ Gesso und Wein auf Leinwand_120x80",
                    "kallus II _ Gesso und Wein auf Leinwand_120x80",
                    "kallus III _ Gesso und Wein auf Leinwand_120x80",
                    "kallus IV _ Gesso und Wein auf Leinwand_120x80",
                    "kallusV _ Gesso und Wein auf Leinwand_120x80",
                   ],
									text: <<-EOT
                  Der Name der Kallusreihe geht auf die Überwallung von Wunden an lebenden Baumstämmen zurück. Undiffrenziert und ungerichtet wachsende Zellen verschließen eine offene Wunde. Schnell muss es gehen. Es geht ums Überleben. Von selbst muss es gehen. Unplanbar. Einem höheren Ziel zugewandt. Komplex. 

Mit Kreide angereicherte Farbe und Rotwein bilden den Grundstock der Kallusreihe. Chemische Reaktionen der Substanzen und ein im richtigen Moment, im richtigen Mischungsverhälltnis aplizierte Schichten auf die noch nicht abgebundenen, verursacht die Transformation der Substanzen. Die Morphologie der Kalli bildet sich erst nach Tagen letztendlich.

EOT

                    
                },
                ul: {
                  title: "unterirdische landschaften",
                  folder: "ul",
                  image_count: 6,
                  #image_postfix: "_oct.jpg",
                  alts: [
                    "unterirdische landschaften I _ Acryl auf Leinwand_120x80",
                    "unterirdische landschaften II _ Mixed Media auf Leinwand_120x80",
                    "unterirdische landschaften III _ Acryl auf Leinwand_100x70",
                    "unterirdische landschaften IV _ Acryl und Caffee auf Leinwand_80x80",
                    "unterirdische landschaften V _ Acryl und Marker auf Leinwand_80x80",
                    "aktivkohle-ammoniumkarbonat _ Mixed Media auf Leinwand_100x160",
                   ],
									text: <<-EOT
                  Unterirdische landschaften
Die Reihe begann mit einer Zusammenarbeit mit dem Videokünstler und Filmemacher Johannes Plank ( http://johannesplank.com ). Ende 2018 entstand dabei die Mehrkanalinstallation „UNORTE“. Diese beinhaltet diverses Videomaterial aus einem alten Uranbergwerk  und einem gleichnamigen Triptychon in 314x160 auf Leinwand. 

Im Rahmen dieser Zusammenarbeit entstand auch das Musikvideo OMR der Band WAELDER (<iframe width="560" height="315" src="https://www.youtube.com/embed/h8EmrgYbAyM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>).

Es geht in der Reihe um die Findung eines ubiquitär verstehbaren Landkartennarrativs. Wie in Bergbaukarten aus verschiedenen Zeitaltern versuchen die Arbeiten einen tomografischen Blick in verschiedene Ebenen. Dabei beziehen sich diese Ebenen auf den immer währenden Widerstreit zwischen Technik und Natur, Entwicklung,Fortschritt und Verfall, Helligkeit und Dunkelheit. Durch den Bezug auf die Montanindustrie skizziert diese Reihe die Frage, warum Gebiete, die bezogen auf den Reichtum an Bodenschätzen wirtschaftlich und kulturell wichtige Positionen im globalen Wettstreit  innehaben könnten – am Ende meist strukturschwach, überdurchschnittlich von existenziellen Nöten bedroht sind und auch psychische Erkrankungen auffallend überproportional in der lokalen Bevölkerung auftreten. Im Hinblick auf die aktuelle politische Situation in vielen Europäischen Städten fällt auf, dass diese Frage trotz ihrer zeitenüberdauernden Relevanz eine ganz aktuelle ist.


EOT
                    
                },
              },
            }


  },
  :bio => { title: "Biografie",
            file: "bio" 

          },
  :index => { title: "",
              file: "index",
          },
  :exib => { title: "",
              file: "exib",
          },
  :new => { title: "",
              file: "new",
          },
  :contact => { title: "",
              file: "kontakt",
          },
  :impressum => { title: "",
              file: "imp",
          },
}


#
# FILTERS
# Run before every action
#
before do
end

#
#ACTIONS
#

# INDEX - Show Overview
get "/" do
  
  erb :index, layout: :default_layout
end

get "/:page" do
  page_name = params[:page]
  @page = pages[page_name.to_sym]
  @data = pages[page_name.to_sym][:data]
  erb @page[:file].to_sym, layout: :default_layout
end

#
# HELPERS
#
helpers do 
end
